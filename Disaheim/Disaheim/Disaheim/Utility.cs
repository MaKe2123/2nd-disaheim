﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class Utility
    {
        public double GetValueOfBook(Book book)
        {
            return book.Price;
        }

        public double GetValueOfAmulet(Amulet amulet)

        {
            double priceAmulet = 0;
            if (amulet.Quality == Level.low)
            {
                priceAmulet = 12.5;
            }

            else if (amulet.Quality == Level.medium)
            {
                priceAmulet = 20.0;
            }

            else if (amulet.Quality == Level.high)
            {
                priceAmulet = 27.5;
            }

            return priceAmulet;
        }

        public double GetValueOfCourse(Course course)
        {
            int hours = course.DurationInMinutes / 60;

            int minutes = course.DurationInMinutes % 60;

            double courseValue = 0;

            if ( minutes == 0)
            {
                courseValue = hours * 875.0;   
            } 
            
            else 
            {
                courseValue = (hours + 1) * 875.0;
            }

            return courseValue; 
        }

    }
}

